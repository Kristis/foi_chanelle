$(document).ready(function () {
  $('.sidenav').sidenav({
    edge: "right"
  });
  $('.modal').modal();
  $('.modal-close').click(function () {
    $('.modal').remove();
  });
});
