$(document).ready(function () {
    $('#firstTab').addClass('active');
    $('.active a').css('text-decoration', 'underline')
    $('.tab').on("click", function () {
        $('.active a').css('text-decoration', 'none')
        $('.tab').removeClass('active')
        $(this).addClass('active')
        $('.active a').css('text-decoration', 'underline')
    })
    $('#btnGenerate').on("click", function () {
        if ($('#firstTab').is('.active')) {
            var rnd = Math.floor(Math.random() * 3)
            var output
            if (rnd == 0) {
                var pronounIndex = Math.floor(Math.random() * pronounArr.length)
                output = pronounArr[pronounIndex]
            } else if (rnd == 1) {
                var wordIndex = Math.floor(Math.random() * nouns.length)
                output = nouns[wordIndex].getDeclen()
            } else if (rnd == 2) {
                var pronounIndex = Math.floor(Math.random() * pronounArr.length)
                var verbIndex = Math.floor(Math.random() * verbs.length)
                output = verbs[verbIndex].getTense(pronounIndex)
            }
            $('#textOutput').html(output)
        } else if ($('#secondTab').is('.active')) {
            var pronounIndex = Math.floor(Math.random() * pronounArr.length)
            var verbIndex = Math.floor(Math.random() * verbs.length)
            var wordIndex = Math.floor(Math.random() * nouns.length)
            var pronounFormatted = pronounArr[pronounIndex].charAt(0).toUpperCase() + pronounArr[pronounIndex].slice(1)
            $('#textOutput').html(pronounFormatted + " " + verbs[verbIndex].getTense(pronounIndex) + " " + nouns[wordIndex].getDeclen() + ".")
        }
        // TODO too big on fullscreen
        $('#textOutputContainer #textOutput').css('font-size', 0);
        var fontSize = parseInt($('#textOutputContainer').height()) + 'px';
        $('#textOutputContainer #textOutput').css('font-size', fontSize);

        // RAVE
        $('#btnGenerate').css('background-color', rndColor())
        $('#firstTab').css('background-color', rndColor())
        $('#secondTab').css('background-color', rndColor())
        $('#textOutputContainer').css('border-color', rndColor())
        $('body').css('background-color', rndColor())

        function rndColor() {
            return colors[Math.floor(Math.random() * colors.length)]
        }
        console.log(rndColor());
    })
})
