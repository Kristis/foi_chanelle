$(document).ready(function () {
	$('.parallax').parallax();
	$('.collapsible').collapsible({ accordion: false });
	$('.sidenav').sidenav({
		edge: "right"
	});
	function rndColor() {
		return colors[Math.floor(Math.random() * colors.length)]
	}
	var ctx = document.getElementById("myChart");
	var myChart = new Chart(ctx, {
		type: 'polarArea',
		data: {
			labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
			datasets: [{
				label: '# of Votes',
				data: [12, 19, 3, 5, 2, 3],
				backgroundColor: [
					rndColor(),
					rndColor(),
					rndColor(),
					rndColor(),
					rndColor(),
					rndColor()
				],
				borderColor: [
					rndColor(),
					rndColor(),
					rndColor(),
					rndColor(),
					rndColor(),
					rndColor()
				],
				borderWidth: 1
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});
});